import React,{Component} from 'react';
class NewMatch extends Component{
    submitBtn=(teamA,teamB)=>{
        if(!teamA&&!teamB){
            alert("Enter the Teams");
        }
        else if(!teamA){
            alert("Enter Team A");
        }
        else if(!teamB){
            alert("Enter Team B");
        }
        else if(teamA===teamB){
            alert("Enter different teams")
        }
        else{
            this.props.onSubmit(1,teamA,teamB);
        }
    }
    setTeam=(num,index)=>{
        let {teams,currentTeam} = this.props;
        let {teamA,teamB}=currentTeam;
        if(num===1){
            teamA=teams[index];
        }
        else{
            teamB=teams[index];
        }
        this.props.onSubmit(0,teamA,teamB);
    }
    render(){
        let {teams,currentTeam}=this.props;
        let {teamA,teamB}=currentTeam;
        return(
            <React.Fragment>
            <h3 className="text-center">{!teamA?"Choose Team 1":"Team 1 : "+teamA}</h3>
            <div className="d-flex justify-content-center">{
                teams.map((x,index)=>{
                    return(
                    <button className="btn-warning m-4"
                    onClick={()=>this.setTeam(1,index)}>{x}</button>
                    )})
                }
            </div>
            <h3 className="text-center">{!teamB?"Choose Team 2":"Team 2 : "+teamB}</h3>
            <div className="d-flex justify-content-center">{
                teams.map((x,index)=>{
                return(
                    <button className="btn-warning m-4"
                    onClick={()=>this.setTeam(2,index)}>{x}</button>
                )
                    })
            }
            </div>
            <div className="text-center">
                <button className="btn btn-dark"
                onClick={()=>this.submitBtn(teamA,teamB)}>Start</button>
            </div>
            </React.Fragment>
        )
    }
}
export default NewMatch;