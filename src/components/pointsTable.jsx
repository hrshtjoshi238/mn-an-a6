import React,{Component} from "react";
class PointsTable extends Component{
    render(){
        let {pointsTable}=this.props;
        return(
            <React.Fragment>
                <div className="row text-center bg-dark text-white">
                    <div className="col-3" onClick={()=>this.props.sortPointsTable(0)}>Team</div>
                    <div className="col-1" onClick={()=>this.props.sortPointsTable(1)}>Played</div>
                    <div className="col-1" onClick={()=>this.props.sortPointsTable(2)}>Won</div>
                    <div className="col-1" onClick={()=>this.props.sortPointsTable(3)}>Lost</div>
                    <div className="col-1" onClick={()=>this.props.sortPointsTable(4)}>Drawn</div>
                    <div className="col-2" onClick={()=>this.props.sortPointsTable(5)}>Goal For</div>
                    <div className="col-2" onClick={()=>this.props.sortPointsTable(6)}>Goal Against</div>
                    <div className="col-1" onClick={()=>this.props.sortPointsTable(7)}>Points</div>
                </div>
                {pointsTable.map((x)=><div className="row text-center border">
                    <div className="col-3">{x.team}</div>
                    <div className="col-1">{x.played}</div>
                    <div className="col-1">{x.won}</div>
                    <div className="col-1">{x.lost}</div>
                    <div className="col-1">{x.tie}</div>
                    <div className="col-2">{x.goalsFor}</div>
                    <div className="col-2">{x.goalsAgainst}</div>
                    <div className="col-1">{x.points}</div>
                </div>)}
            </React.Fragment>
        )
    }
}
export default PointsTable;