import React,{Component} from 'react';
class InMatch extends Component{
    render(){
        let {currentTeam,currentScore}=this.props;
        let {scoreA,scoreB}=currentScore;
        let {teamA,teamB}=currentTeam;
        return(<React.Fragment>
            <h3 className="text-center">Welcome to an Exciting Match</h3>
            <div className="row">
            
                <div className="col-4 text-center">
                    <h3>{teamA}</h3><br/>
                    <button className="btn btn-warning"
                    onClick={()=>this.props.handleScore(1)}>Goal Scored</button>
                </div>
                <div className="col-4 text-center">
                    <h3>{scoreA}:{scoreB}</h3>
                </div>
                <div className="col-4 text-center">
                    <h3>{teamB}</h3><br/>
                    <button className="btn btn-warning"
                    onClick={()=>this.props.handleScore(2)}>Goal Scored</button>
                </div>
            </div>
            <div className="text-center"><button className="btn btn-warning"
            onClick={()=>this.props.matchOver()}>Match Over</button></div>
            </React.Fragment>
        )
    }
}
export default InMatch;