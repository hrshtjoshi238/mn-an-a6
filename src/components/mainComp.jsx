import React,{Component} from 'react';
import AllMatchs from './allMatch';
import InMatch from './inMatch';
import NavBar from './navBar';
import NewMatch from './newMatch';
import PointsTable from './pointsTable';
class MainComp extends Component{
    state={
        teams:["France","England","Brazil","Germany","Argentina"],
        pointsTable:[
            {team:"France",played:0,won:0,lost:0,tie:0,goalsFor:0,goalsAgainst:0,points:0},
            {team:"England",played:0,won:0,lost:0,tie:0,goalsFor:0,goalsAgainst:0,points:0},
            {team:"Brazil",played:0,won:0,lost:0,tie:0,goalsFor:0,goalsAgainst:0,points:0},
            {team:"Germany",played:0,won:0,lost:0,tie:0,goalsFor:0,goalsAgainst:0,points:0},
            {team:"Argentina",played:0,won:0,lost:0,tie:0,goalsFor:0,goalsAgainst:0,points:0},
        ],
        currentTeam:{teamA:"",teamB:""},
        currentScore:{scoreA:0,scoreB:0},
        matchList:[],
        matchStart:false,
        view:-1
    }
    changeView=(num)=>{
        let s1={...this.state};
        if(num===2){
            s1.matchStart=true;
            s1.currentTeam={teamA:"",teamB:""};
            s1.currentScore={scoreA:0,scoreB:0};
        }
        s1.view=num;
        this.setState(s1);
    }
    handleSubmit=(num,teamA,teamB)=>{
        let s1={...this.state};
        if(num===0){
            s1.currentTeam.teamA=teamA;
            s1.currentTeam.teamB=teamB;
        }
        else{
            s1.currentTeam.teamA=teamA;
            s1.currentTeam.teamB=teamB;
            s1.view=3;
        }
        this.setState(s1);
    }
    matchOver=()=>{
        console.log("Hello!Match Over");
        let s1={...this.state};
        let pointsTable=[...s1.pointsTable];
        s1.view=-1;
        s1.matchStart=false;
        s1.pointsTable=pointsTable.map((x)=>{
            if(x.team===s1.currentTeam.teamA||x.team===s1.currentTeam.teamB){
                x.played++;
                if(x.team===s1.currentTeam.teamA){
                    x.goalsFor+=s1.currentScore.scoreA;
                    x.goalsAgainst+=s1.currentScore.scoreB;
                    if(s1.currentScore.scoreA>s1.currentScore.scoreB){
                        x.won++;
                        x.points+=3;
                    }
                    else if(s1.currentScore.scoreA===s1.currentScore.scoreB){
                        x.tie++;
                        x.points++;
                    }
                    else{
                        x.lost++;
                    }
                }
                else{
                    x.goalsFor+=s1.currentScore.scoreB;
                    x.goalsAgainst+=s1.currentScore.scoreA;
                    if(s1.currentScore.scoreA<s1.currentScore.scoreB){
                        x.won++;
                        x.points+=3;
                    }
                    else if(s1.currentScore.scoreA===s1.currentScore.scoreB){
                        x.tie++;
                        x.points++;
                    }
                    else{
                        x.lost++;
                    }
                }
            }
            return x;
        })
        let jsON={
            team1:{name:s1.currentTeam.teamA,score:s1.currentScore.scoreA},
            team2:{name:s1.currentTeam.teamB,score:s1.currentScore.scoreB},
            result:(
                s1.currentScore.scoreA>s1.currentScore.scoreB?s1.currentTeam.teamA+" Won"
                :s1.currentScore.scoreA<s1.currentScore.scoreB?s1.currentTeam.teamB+" Won"
                :"Tie"
            )
        }
        s1.matchList.push(jsON);
        this.setState(s1);
    }
    handleScore=(num)=>{
        let s1={...this.state};
        if(num===1){
            s1.currentScore.scoreA++;
        }
        else{
            s1.currentScore.scoreB++;
        }
        this.setState(s1);
    }
    sortPointsTable=(num)=>{
        let s1={...this.state}
        num===0?s1.pointsTable.sort((x,y)=>x.team.localeCompare(y.team))
        :num===1?s1.pointsTable.sort((x,y)=>y.played-x.played)
        :num===0?s1.pointsTable.sort((x,y)=>y.won-x.won)
        :num===0?s1.pointsTable.sort((x,y)=>y.lost-x.lost)
        :num===0?s1.pointsTable.sort((x,y)=>y.tie-x.tie)
        :num===0?s1.pointsTable.sort((x,y)=>y.goalsFor-x.goalsFor)
        :num===0?s1.pointsTable.sort((x,y)=>y.goalsAgainst-x.goalsAgainst)
        :s1.pointsTable.sort((x,y)=>y.points-x.points);
        this.setState(s1);
    }
    render(){
        let {teams,matchList,view,currentTeam,currentScore,matchStart,pointsTable}=this.state;
        let team={teamA:matchStart&&currentTeam.teamA?currentTeam.teamA:"",teamB:matchStart&&currentTeam.teamB?currentTeam.teamB:""};
        let score={scoreA:matchStart&&currentTeam.teamA?currentScore.scoreA:0,scoreB:matchStart&&currentScore.scoreB?currentScore.scoreB:0};
        return(
            <React.Fragment>
                <NavBar
                totalMatch={matchList.length}/>
                <div className="container pt-3">
                {   view!=3?<React.Fragment><button className="btn btn-primary m-2"
                    onClick={()=>this.changeView(0)}>All Matches</button>
                    <button className="btn btn-primary m-2"
                    onClick={()=>this.changeView(1)}>Points Table</button>
                    <button className="btn btn-primary m-2"
                    onClick={()=>this.changeView(2)}>New Match</button></React.Fragment>:<InMatch
                    currentTeam={currentTeam}
                    currentScore={score}
                    handleScore={this.handleScore}
                    matchOver={this.matchOver}/>
                } 
                {
                    view===2?<NewMatch
                    teams={teams}
                    currentTeam={team}
                    onSubmit={this.handleSubmit}/>
                    :view===0?
                    <AllMatchs
                    matchList={matchList}/>
                    :view===1?<PointsTable
                    pointsTable={pointsTable}
                    sortPointsTable={this.sortPointsTable}/>:""
                }
                </div>
            </React.Fragment>
        )
    }
}
export default MainComp; 