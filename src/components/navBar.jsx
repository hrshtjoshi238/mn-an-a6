import React, {Component} from 'react';
class NavBar extends Component{
    render(){
        let {totalMatch}=this.props;
        return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                <a className="navbar-brand" href="#">
                    FootBall Tournament
                </a>
                <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-taget="#navbarSupportedContent">
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul className="navbar-nav mr-auto" href="#">
                        <li className="nav-item">
                            <a className="nav-link" href="#">
                                Number of Matches
                                <span className="badge badge-pill badge-primary">
                                    {totalMatch}
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </nav>
        );
    }
}
export default NavBar;