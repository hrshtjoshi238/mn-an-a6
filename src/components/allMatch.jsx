import React,{Component} from 'react';
class AllMatchs extends Component{
    render(){
        let {matchList}=this.props;
        return(
            <React.Fragment>
                <div className="row text-center bg-dark text-white">
                    <div className="col-3">Team 1</div>
                    <div className="col-3">Team 2</div>
                    <div className="col-3">Score</div>
                    <div className="col-3">Result</div>
                </div>
                {matchList.map((x)=><div className="row text-center border">
                    <div className="col-3">{x.team1.name}</div>
                    <div className="col-3">{x.team2.name}</div>
                    <div className="col-3">{x.team1.score}-{x.team2.score}</div>
                    <div className="col-3">{x.result}</div>
                </div>)}
            </React.Fragment>
        )
    }
}
export default AllMatchs;